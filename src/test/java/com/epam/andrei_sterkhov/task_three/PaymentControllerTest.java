package com.epam.andrei_sterkhov.task_three;

import com.epam.andrei_sterkhov.task_threee.AccountService;
import com.epam.andrei_sterkhov.task_threee.DepositService;
import com.epam.andrei_sterkhov.task_threee.InsufficientFundsException;
import com.epam.andrei_sterkhov.task_threee.PaymentController;
import org.junit.Before;
import org.junit.Test;
import org.mockito.AdditionalMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

public class PaymentControllerTest {
    @Mock
    private AccountService accountService;
    @Mock
    private DepositService depositService;

    @InjectMocks
    private PaymentController controller;

    @Before
    public void init() throws InsufficientFundsException {
        MockitoAnnotations.initMocks(this);
        doThrow(InsufficientFundsException.class).when(depositService).deposit(AdditionalMatchers.geq(100L), anyLong());
        when(accountService.isUserAuthenticated(100L)).thenReturn(true);
    }

    @Test
    public void checkedThatCallIsUserAuthenticatedWithDefinedParameter() throws InsufficientFundsException {
        controller.deposit(50L, 100L);
        verify(accountService).isUserAuthenticated(eq(100L));
    }

    @Test(expected = SecurityException.class)
    public void failedDepositForAnauthenticatedUser() throws InsufficientFundsException {
        controller.deposit(40L, 90L);
    }

    @Test(expected = InsufficientFundsException.class)
    public void failedDepositOfLargeAmount() throws InsufficientFundsException {
        controller.deposit(100L, 100L);
    }
}
