package com.epam.andrei_sterkhov.task_two;

import org.junit.Before;
import org.junit.Test;

public class CalculatorNegativeTests {
    private Calculator calculator;

    @Before
    public void init() {
        calculator = new Calculator();
    }

    @Test(expected = ArithmeticException.class)
    public void divideByZero() {
        calculator.getResult("1/0");
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyString() {
        calculator.getResult("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void incorrectBrackets() {
        calculator.getResult("7)+(8");
    }

    @Test(expected = IllegalArgumentException.class)
    public void oddCountBrackets() {
        calculator.getResult("(7+8))");
    }

    @Test(expected = IllegalArgumentException.class)
    public void moreOneSignsInRow() {
        calculator.getResult("7++5");
    }

    @Test(expected = IllegalArgumentException.class)
    public void incorrectCharsInString() {
        calculator.getResult("7+5=");
    }
}
