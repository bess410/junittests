package com.epam.andrei_sterkhov.task_two;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class CalculatorPositiveTests {
    private Calculator calculator;

    @Before
    public void init() {
        calculator = new Calculator();
    }

    @Test
    public void addition() {
        assertThat(4.0, is(calculator.getResult("2+2")));
    }

    @Test
    public void withoutAction() {
        assertThat(2.0, is(calculator.getResult("2")));
    }

    @Test
    public void substraction() {
        assertThat(1.0, is(calculator.getResult("3-2")));
    }

    @Test
    public void multiplication() {
        assertThat(6.0, is(calculator.getResult("3*2")));
    }

    @Test
    public void division() {
        assertThat(1.5, is(calculator.getResult("3/2")));
    }

    @Test
    public void resultWithBrackets() {
        assertThat(8.0, is(calculator.getResult("(2+2)*2")));
    }

    @Test
    public void resultWithoutBrackets() {
        assertThat(6.0, is(calculator.getResult("2+2*2")));
    }

    @Test
    public void complexExpression() {
        assertThat(2.1428571428571423, is(calculator.getResult("2+2*(2-3/7)-3")));
    }

}