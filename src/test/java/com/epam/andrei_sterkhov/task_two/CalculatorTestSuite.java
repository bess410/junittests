package com.epam.andrei_sterkhov.task_two;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({CalculatorNegativeTests.class, CalculatorPositiveTests.class})
public class CalculatorTestSuite {
}
