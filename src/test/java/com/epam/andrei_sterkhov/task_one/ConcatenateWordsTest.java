package com.epam.andrei_sterkhov.task_one;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ConcatenateWordsTest {
    private Utils utils;

    @Before
    public void init() {
        utils = new Utils();
    }

    @Test
    public void concatenateWords() {
        assertEquals("string1string2", utils.concatenateWords("string1", "string2"));
    }

    @Test
    public void concatenateNulls() {
        assertEquals("nullnull", utils.concatenateWords(null, null));
    }

    @Test
    public void concatenateEmptyStrings() {
        assertEquals("", utils.concatenateWords("", ""));
    }

    @Test
    public void concatenateNonLatin() {
        assertEquals("строка1строка2", utils.concatenateWords("строка1", "строка2"));
    }
}