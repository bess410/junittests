package com.epam.andrei_sterkhov.task_one;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

public class UtilsTestRunner {
    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(UtilsTestSuite.class);

        result.getFailures().forEach(System.out::println);

        System.out.println(result.wasSuccessful());
    }
}
