package com.epam.andrei_sterkhov.task_one;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runners.model.TestTimedOutException;

import java.math.BigInteger;

import static org.junit.Assert.*;

public class ComputeFactorialTest {
    private Utils utils;

    private static BigInteger result;

    @BeforeClass
    public static void initResult() {
        result = new Utils().computeFactorial(5000);
    }

    @Before
    public void init() {
        utils = new Utils();
    }

    @Test
    public void computeFactorial() {
        assertEquals(BigInteger.valueOf(720), utils.computeFactorial(6));
    }

    @Test
    public void computeZeroFactorial() {
        assertEquals(BigInteger.valueOf(1), utils.computeFactorial(0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void computeNegativeFactorial() {
        utils.computeFactorial(-1);
    }

    @Test(timeout = 50)
    public void testFactorialWithTimeout() {
        assertEquals(result, utils.computeFactorial(5000));
    }
}