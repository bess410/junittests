package com.epam.andrei_sterkhov.task_threee;

public interface DepositService {
    String deposit(Long amount, Long userId) throws InsufficientFundsException;
}
