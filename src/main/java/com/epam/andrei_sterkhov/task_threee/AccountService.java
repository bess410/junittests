package com.epam.andrei_sterkhov.task_threee;

/**
 * Created by Kudryavtsev Alexander on 25.02.2015.
 */
public interface AccountService {
    boolean isUserAuthenticated(Long userId);
}
