package com.epam.andrei_sterkhov.task_two;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
    private boolean isValidateWithoutBrackets(String string) {
        Pattern p = Pattern.compile("^([\\-]?[1-9][\\d]*|[\\d])(([/\\-+*])([1-9][\\d]*|[\\d]))*$");
        Matcher m = p.matcher(string);
        return m.matches();
    }

    private boolean isValidateWithBrackets(String string) {
        // Разобрать выражение по скобкам
        //-(-(2+40)/(5-33)*2)
        // Проверить каждое isValidateWithoutBrackets
        while (true) {
            // Ищем закрывающуюся скобку
            int closeIndex = string.indexOf(')');
            // Ищем открывающую скобку перед нашей закрывающей
            int openIndex = string.lastIndexOf('(', closeIndex);
            // Если нет скобок то возвращаем результат валидации строки без скобок
            if (closeIndex == -1 && openIndex == -1) {
                return isValidateWithoutBrackets(string);
            }
            // Если закрывающаяся скобка нашлась, а открывающейся нет или она стоит после закрывающейся
            if (closeIndex != -1 && openIndex == -1) {
                return false;
            }
            // Получаем первую часть нашей строки, исключая открывающую скобку
            String first = string.substring(0, openIndex);
            // Получаем вторую часть нашей строки, исключая закрывающую скобку
            String second = string.substring(closeIndex + 1, string.length());
            // Если скобка стоит между цифр, возвращаем false
            if (first.length() != 0 && Character.isDigit(first.charAt(first.length() - 1))) {
                return false;
            }
            // Если скобка стоит между цифр, возвращаем false
            if (second.length() != 0 && Character.isDigit(second.charAt(0))) {
                return false;
            }

            // Получаем выражение в скобках
            String tempExpression = string.substring(openIndex + 1, closeIndex);
            // Проверяем на валидность
            if (!isValidateWithoutBrackets(tempExpression)) {
                return false;
            }

            int result = 1; //Если выражение валидно то устанавливаем значение в скобках равным 1
            // Склеиваем наши строки
            string = first + result + second;
        }
    }

    public void checkExpression(String expression) {
        if (!isValidateWithBrackets(expression)) {
            throw new IllegalArgumentException("Incorrect expression");
        }
    }
}
