package com.epam.andrei_sterkhov.task_one;

import java.math.BigInteger;

public class Utils {
    public String concatenateWords(String str1, String str2) {
        return str1 + str2;
    }

    public BigInteger computeFactorial(long n){
        if (n < 0) {
            throw new IllegalArgumentException("Negative argrument is not allowed.");
        }
        if (n == 0) return BigInteger.valueOf(1);
        return BigInteger.valueOf(n).multiply(computeFactorial(n - 1));
    }
}
